# React E-Commerce 

E-commerce website using React & Redux   
Mathis DYK & Isabelle REGNIER 

---

**Front technologies:** React, Redux  
**Auth:** JWT  
**Storage:** JSON-Server  

## Project setup

> Run the following commands from `reactecommerce` root directory

### Install dependencies

```
npm install

``` 

### Start the app

```
npm run start
npm run json-server

```

The front app will be running at http://localhost:3000/  
Json-Server will be running at http://localhost:3001

## Features

### Accueil
- [x] Afficher les trois derniers articles  

*Bonus*
- [ ] *Afficher toutes les catégories (seule la personne ayant cree la categorie peut la supprimer)*

### Profil
- [x] Afficher toutes les informations de l'utilisateur (nom, prénom, âge, photo de profil)
- [x] Bouton permettant à l'utilisateur de modifier son profil

### Articles
- [x] Pagination
- [x] Seule la personne ayant cree son article peut le supprimer  

*Bonus*
- [ ] *Rechercher un article*
- [ ] *Filtrer par catégorie*

### Article
- [x] Afficher les informations (titre, image, description, prix)
- [x] Pouvoir choisir la quantité

### Category
*Bonus*
- [ ] *Creation de categories*
- [ ] *Edition de categories*

### Composants requis
- [x] Header (connexion/deconnexion)
- [x] Footer

### Models
- [x] User: Nom, Prenom, Image(URL), Role, Email, Date de creation
- [x] Article: Nom, Image, Description, Prix, user_id, Date de creation  

*Bonus*  
- [ ] *Category: Nome, Image, user_id*
