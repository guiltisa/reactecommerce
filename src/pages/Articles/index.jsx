import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import ConfirmationPopup from '../../components/ConfirmationPopup'
import ArticlePreview from '../../components/ArticlePreview'
import { getArticles, removeArticleById, retrieveArticles } from '../../store/articles'
import { getCurrentUser } from "../../store/users";
import ReactPaginate from "react-paginate";
import './index.scss';

const Articles = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const articles = useSelector(getArticles)
  const user = useSelector(getCurrentUser);
  const perPage = 3
  const [pageCount, setPageCount] = useState(0)
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    if (!articles.length) {
      dispatch(retrieveArticles())
    }
  })
  
  console.log(articles, offset, perPage);

  const [displayPopup, setDisplayPopup] = useState(false)
  const [articleIdToRemove, setArticleIdToRemove] = useState(false)

  const removeArticle = () => {
    dispatch(removeArticleById(articleIdToRemove))
    setArticleIdToRemove()
    setDisplayPopup(false)
  }

  const handlePageClick = (e) => {
    const selectedPage = e.selected;
    console.log("Selected Page", selectedPage)
    setOffset((selectedPage) * perPage);
  };

  const displayConfirmationPopup = articleId => {
    setArticleIdToRemove(articleId)
    setDisplayPopup(true)
  }

  useEffect(() => {
    setPageCount(Math.ceil(articles.length / perPage));
  }, [offset, articles.length])

  return (
    <section className="flex-column justify-center items-center align-center w-10/12 m-auto">
      {displayPopup && (
        <ConfirmationPopup
          onClose={() => setDisplayPopup(false)}
          confirmed={removeArticle}
        />
      )}
      <h1 className="text-4xl font-bold my-5">Tous les articles</h1>
      {user && (
        // <div className="w-2/3 mx-auto">
        <button
          className="bg-indigo-400 hover:bg-indigo-500 rounded px-5 py-2 text-white"
          onClick={() => history.push("/create-article")}
        >
          Créer un article
        </button>
        // </div>
      )}
      <div className="m-auto flex flex-wrap justify-between mx-auto">
        {articles.slice(offset, offset + perPage).map((article, index) => (
          <ArticlePreview
            key={index}
            article={article}
            displayPopup={displayConfirmationPopup}
          />
        ))}
      </div>
        <ReactPaginate
          previousLabel={"previous"}
          nextLabel={"next"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          onPageChange={handlePageClick}
          pageCount={pageCount}
          marginPagesDisplayed={0}
          pageRangeDisplayed={5}
          containerClassName={"pagination"}
          activeClassName={"active"}
        />
    </section>
  );
}

export default Articles;
