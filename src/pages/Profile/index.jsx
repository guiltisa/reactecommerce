import React , { useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import Auth from "../../hoc/auth";
import Button from "../../components/Button";
import { getCurrentUser } from "../../store/users";
import EditProfile from "../../components/EditProfile"
import './index.scss'
 

const Profile = () => {
  const user = useSelector(getCurrentUser);
  const [profileState, setProfileState] = useState({
    edit: false
  })

  return (
    <>
      <div className="w-full h-screen flex justify-center items-center profile">
        <div className="flex w-full flex-col items-center">
          {profileState.edit === false && (
            <>
            <img src={user?.profilePicture} alt="" width="200" height="200" className="profilepic"/>
              <p className="my-5">
                {user?.firstName} {user?.lastName}            
              </p>
              <Button
                text={"Edit"}
                handleClick={() => setProfileState({ edit: true })}
              />
            </>
          )}
          {profileState.edit && <EditProfile user={user} />}
        </div>
      </div>
    </>
  );
};

export default Auth(Profile);
