import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { createArticle } from '../../store/articles'
import Button from '../../components/Button'
import Textarea from '../../components/Textarea'
import Input from '../../components/Input'
import Banner from '../../components/Banner'
import { useSelector } from "react-redux"
import { getCurrentUser } from "../../store/users";
import Auth from '../../hoc/auth'
import GetBackTo from "../../components/GetBackTo";
import ArrowLeft from "../../static/img/arrow-left.svg";

const CreateArticle = () => {
  const dispatch = useDispatch();
  const user = useSelector(getCurrentUser);
  const history = useHistory();
  const [displayBanner, setDisplayBanner] = useState(false)
  const [fields, setFields] = useState({
    title: "",
    image: "",
    description: "",
    price: "0.00",
    quantity: 0,
    user_id: user ? user.id : "",
    created_date : new Date()
  });

  const handleChangeField = ({ target: { name, value } }) => {
    setFields({ 
      ...fields, 
      [name]: value
    })
  }

  const closeBanner = () => setDisplayBanner(false)

  const submitForm = (e) => {
    e.preventDefault();
    const checkRequired = Object.keys(fields).find(key => fields[key] === '')
    if (checkRequired || fields.price === '0.00') {
      return;
    }
    dispatch(createArticle({...fields}))
    setDisplayBanner(true)
    setFields({
      title: "",
      image: "",
      description: "",
      price: "0.00",
      quantity: 0,
    });
  }

  return (
    <>
    <div className="w-2/3 mx-auto">
      <h1 className="text-center text-4xl font-bold">Ajouter un article</h1>
      <GetBackTo img={ArrowLeft} text={"Retour aux articles"} to={"/articles"} />
    </div>  
      <form onSubmit={submitForm} className="w-2/3 mx-auto">
        {displayBanner && (
          <Banner text="Article ajoute !" close={closeBanner} />
        )}
        <Input
          label="Titre"
          id="title"
          name="title"
          className="mt-10"
          value={fields.title}
          handleChange={handleChangeField}
        />
        <Textarea
          label="Description"
          id="description"
          name="description"
          value={fields.description}
          handleChange={handleChangeField}
        />
        <Textarea
          label="Image"
          id="image"
          name="image"
          value={fields.image}
          handleChange={handleChangeField}
        />
        <Input
          label="Prix"
          id="price"
          name="price"
          step="0.01"
          type="number"
          value={fields.price}
          handleChange={handleChangeField}
        />
        <Input
          label="Quantité"
          id="quantity"
          name="quantity"
          type="number"
          value={fields.quantity.toString()}
          handleChange={handleChangeField}
        />
        <Button type="submit" text="Ajouter l'article"/>
        {/* <Button text="Retour aux articles" handleClick={() => history.push("/articles")}/> */}
      </form>
    </>
  );
}

export default Auth(CreateArticle);
