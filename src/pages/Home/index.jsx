import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getArticles, retrieveArticles } from "../../store/articles";
import ArticlePreview from '../../components/ArticlePreview';
import GetBackTo from "../../components/GetBackTo";

const Home = () => {
  const articles = useSelector(getArticles);
  const dispatch = useDispatch();

  React.useEffect(() => {
    console.log("Articles", articles);
  }, [articles]);

  React.useEffect(() => {
    if (!articles.length) {
      dispatch(retrieveArticles());
    }
  });
  
  return (
    <div className="w-10/12 flex-column justify-between m-auto">
      <h1 className="font-bold mt-6 text-4xl f-32">Accueil</h1>
      <h2 className="font-bold text-2xl mt-6">Proposition d'articles</h2>
      <div className="flex justify-between items-center flex-wrap">
        {articles.slice(0, 3).map((article) => (
          <ArticlePreview key={article.id} article={article} />
        ))}
        <div className="flex w-full">
          <GetBackTo text={"Voir tous les articles"} to="/articles" />
        </div>
        {/* <Link className="block" to="/articles">
        <Button text="Articles" />
      </Link>
      <Link className="block" to="/popup">
        <Button text="Popup" />
      </Link>
      <Link className="block mt-2" to="/form">
        <Button text="Form" />
      </Link>
      <Link className="block mt-2" to="/calculator">
        <Button text="Calculator" />
      </Link>
      <Link className="block mt-2" to="/articles">
        <Button text="Article" />
      </Link>
      <Link className="block mt-2" to="/login">
        <Button text="Twitter" />
      </Link> */}
      </div>
    </div>
  );
}

export default Home;
