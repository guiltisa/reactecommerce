import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import Calculator from './pages/Calculator';
import Home from './pages/Home'
import Form from './pages/Form';
import Popup from './pages/Popup';
import Articles from './pages/Articles';
import Article from './pages/Article';
import Register from './pages/Register';
import Login from './pages/Login';
import Posts from './pages/Posts';
import CreatePost from './pages/CreatePost'
import Profile from './pages/Profile';

import CreateArticle from './pages/CreateArticle';
import './tailwind.output.css'
import Header from './components/Header';
import Footer from './components/Footer';

const App = () => (
  <Router>
    <Header />
    <Switch>
      <Route path="/calculator" component={Calculator} />
      <Route path="/form" component={Form} />
      <Route path="/popup" component={Popup} />
      <Route path="/create-article" component={CreateArticle} />
      <Route path="/articles/:id" component={Article} />
      <Route path="/articles" component={Articles} />
      <Route path="/register" component={Register} />
      <Route path="/login" component={Login} />
      <Route path="/posts" component={Posts} />
      <Route path="/create-post" component={CreatePost} />
      <Route path="/profile" component={Profile} />
      <Route path="/" component={Home} />
    </Switch>
    <Footer />
  </Router>
);

export default App;
