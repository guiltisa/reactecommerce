import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from "prop-types";

const GetBackTo = ({ img, text, to }) => (
  <Link className="flex underline mb-5" to={to}>
    {img && <img src={img} alt="" className="mr-2" />} {text}
  </Link>
)

GetBackTo.propTypes = {
  img: PropTypes.string,
  text: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

GetBackTo.defaultProps = {
  img: "",
  text: "",
  to: "/"
};

export default GetBackTo
