import Cookies from 'js-cookie'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom';
import { getCurrentUser, resetProfile } from '../../store/users'
import './index.scss'
import Home from "../../static/img/home.svg";
import GetBackTo from '../GetBackTo';

const Header = () => {
  const user = useSelector(getCurrentUser);
  const history = useHistory();
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(resetProfile())
    Cookies.remove('jwt')
    history.push('/login')
  }

  return (
    <header className="mb-5 w-full h-20 px-5 flex flex-col items-end justify-center bg-indigo-800">
      <div className="flex w-full text-white centeredvert brrr">
        <div className="justify-center h20 homebtn">
          <GetBackTo to={"/"} img={Home} text={""}/>
        </div>
        <div className="maintitle">
          <h1>React E-Commerce</h1>
        </div>
      {user === null ? (
        <div className="flex text-white centeredvert rightcont">
          <button
            className="py-2 mr-2 px-4 border-white border-2 rounded text-white"
            onClick={() => history.push("/login")}
          >
            Se connecter
          </button>
          <button
            className="py-2 px-4 border-white border-2 rounded text-white"
            onClick={() => history.push("/register")}
          >
            S'inscrire
          </button>
        </div>
      ) : (
        <div className="flex text-white centeredvert rightcont">
          <div className="flex">
            <img src={user?.profilePicture} alt="" width="50" height="50" className="profilepic mx-2"/>
            <Link
              className="py-2 px-4 border-white mr-2 border-2 rounded text-white profilebtn hover:bg-indigo-500"
              to={"/profile"}
            >
              {user.username}
            </Link>
            <button
              className="py-2 px-4 border-white border-2 rounded text-white hover:bg-red-500"
              onClick={logout}
            >
              Deconnexion
            </button>
          </div>
        </div>
      )}
      </div>
    </header>
  );
}

export default Header;
