import PropTypes from 'prop-types';
import { useSelector } from 'react-redux'
import { getCurrentUser } from '../../store/users'

const ArticlePresentation = ({ article, deleteArticle }) => {
  const user = useSelector(getCurrentUser)
  return (
    <div className="relative w-full border p-3 mb-2">
      <p className="font-semibold">{article.image}</p>
      <p>{article.title}  ${article.price}€</p>
      {user?.id === article.user.id && <button className="absolute top-2 right-2" onClick={() => deleteArticle(article.id)}>x</button>}
    </div>
  )
}

ArticlePresentation.propTypes = {
  article: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    user: PropTypes.shape({
      id: PropTypes.number.isRequired
    })
  }).isRequired,
  deleteArticle: PropTypes.func.isRequired
}

export default ArticlePresentation;
