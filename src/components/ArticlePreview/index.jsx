import React from 'react'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { getCurrentUser } from "../../store/users";
import './index.scss';

const ArticlePreview = ({ article, displayPopup }) => {
  const user = useSelector(getCurrentUser);
  const history = useHistory();

  const displayConfirmationPopup = e => {
    e.stopPropagation();
    displayPopup(article.id)
  }

  React.useEffect(() => {
    console.log(article);
  }, [article]);

  return (
    <div
      className="w-1/4 m-10 flex flex-col items-center pb-5 h-50 border cursor-pointer articlePreview hover:border-indigo-500"
      onClick={() => history.push(`/articles/${article.id}`)}
    >
      <img className="mb-5" src={article.image} alt="" />
      <p className="text-center">
        <b>{article.title}</b>
      </p>
      <p className="text-center">{article.price}€</p>
      {user && user?.id === article.user_id && displayPopup && (
        <button className="delete" onClick={displayConfirmationPopup}>
          x
        </button>
      )}
    </div>
  );
}

ArticlePreview.propTypes = {
  displayPopup: PropTypes.func,
  article: PropTypes.shape({
    id: PropTypes.number.isRequired,
    price: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  })
}

ArticlePreview.defaultProps = {
  displayPopup: null
}

export default ArticlePreview;
