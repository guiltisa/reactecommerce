import React from 'react'

const Footer = () => {
  return (
    <footer>
      <div className="container mx-auto px-6">
        <div className="mt-16 border-gray-300 flex flex-col items-center">
          <div className="sm:w-2/3 text-center py-6">
            <p className="text-sm text-700 font-bold mb-2">
              © 2021 by Mathis DYK & Isabelle Regnier 
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer
