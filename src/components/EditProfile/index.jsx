import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Button from "../Button";
import Input from "../Input";
import { editUser, getCurrentUser } from "../../store/users";

const EditProfile = () => {
  const user = useSelector(getCurrentUser);
  const dispatch = useDispatch();
  const history = useHistory();
  const [fields, setFields] = useState({
    firstName: user.firstName || "",
    lastName: user.lastName || "",
    username: user.username || "",
    password: "",
    age: user.age || "",
    email : user.email || "",
    profilePicture: user.profilePicture || "",
  });

  const handleChangeField = ({ target: { name, value } }) =>
    setFields({ ...fields, [name]: value });

  const submitForm = async (e) => {
    e.preventDefault();

    const checkError = Object.keys(fields).find((field) => field === "");

    if (checkError) {
      return;
    }

    dispatch(editUser(fields));
    history.push("/login");
  };

  return (
    <>
      <h1 className="text-center py-10 font-bold text-2xl my-5">Edit profile</h1>
      <form onSubmit={submitForm} className="w-1/3 m-auto rounded p-5">
        <Input
          label="Prénom"
          id="firstName"
          name="firstName"
          value={fields.firstName}
          handleChange={handleChangeField}
        />
        <Input
          label="Nom"
          id="lastName"
          name="lastName"
          value={fields.lastName}
          handleChange={handleChangeField}
        />
        <Input
          label="Adresse email"
          id="email"
          name="email"
          value={fields.email}
          handleChange={handleChangeField}
        />
        <Input
          label="Age"
          id="age"
          name="age"
          value={fields.age}
          handleChange={handleChangeField}
        />
        <Input
          label="Profile picture URL"
          id="profilePicture"
          name="profilePicture"
          value={fields.profilePicture}
          handleChange={handleChangeField}
        />
        <Input
          label="Mot de passe"
          id="password"
          type="password"
          name="password"
          value={fields.password}
          handleChange={handleChangeField}
        />

        <Button type="submit" text="Valider" />
      </form>
    </>
  );
};

export default EditProfile;
