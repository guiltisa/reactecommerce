import React from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import ArticlePreview from '../ArticlePreview'
import ArrowLeft from "../../static/img/arrow-left.svg";
import { getNextArticle, getPreviousArticle } from '../../store/articles'
import GetBackTo from "../GetBackTo";
import { getCurrentUser } from "../../store/users";
import './index.scss';

const ArticleDescription = ({ article }) => {
  const history = useHistory();
  const nextArticle = useSelector(state => getNextArticle(state, article.id))
  const previousArticle = useSelector(state => getPreviousArticle(state, article.id))
  const user = useSelector(getCurrentUser);

  return (
    <div className="px-10">
      <GetBackTo
        img={ArrowLeft}
        text={"Voir tous les articles"}
        to={"/articles"}
      />
      <div className="flex">
        <img className="w-1/2 mb-10" src={article.image} alt="" />
        <div className="w-1/2 pl-12">
          <h1 className="mb-10 font-semibold text-4xl my-5">{article.title}</h1>
          <h3>Description</h3>
          <p className="mb-5">{article.description}</p>
          <h3>Disponibilité</h3>
          <p className="mb-5">
            {article.quantity > 0 ? "En stock" : "Rupture de stock"}
          </p>
          <h3 className="mb-2">À partir de</h3>
          <p className="font-semibold text-5xl">{article.price}€</p>
        </div>
      </div>
      <h2 className="text-2xl font-semibold">Plus d'articles...</h2>
      <div className="flex justify-evenly">
        {previousArticle && <ArticlePreview article={previousArticle} />}
        {nextArticle && <ArticlePreview article={nextArticle} />}
      </div>
      {article?.user_id === user?.id ? <p>Oui</p> : null}
    </div>
  );
}

ArticleDescription.propTypes = {
  article: PropTypes.shape({
    id: PropTypes.number.isRequired,
    price: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  })
}

export default ArticleDescription;
